/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import com.mycompany.lab3.Lab3;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OxProgramUnitTest {
    
    public OxProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void  testCheckWinNoPlayBy_O (){
        Lab3.board = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        Lab3.currentPlayer = 'O';
        assertEquals(false, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinRow2By_O_output_true (){
        Lab3.board = new char[][]{{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}}; 
        Lab3.currentPlayer = 'O';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinRow1By_X_output_true (){
        Lab3.board = new char[][]{{'X', 'X', 'X'}, {'-', 'O', 'O'}, {'-', '-', '-'}}; 
        Lab3.currentPlayer = 'X';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinRow3By_X_output_true (){
        Lab3.board = new char[][]{{'-', 'O', 'O'}, {'-', '-', '-'}, {'X', 'X', 'X'}}; 
        Lab3.currentPlayer = 'X';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinCol1By_X_output_true (){
        Lab3.board = new char[][]{{'X', 'O', 'O'}, {'X', '-', '-'}, {'X', 'O', 'X'}}; 
        Lab3.currentPlayer = 'X';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinCol2By_O_output_true (){
        Lab3.board = new char[][]{{'X', 'O', 'O'}, {'X', 'O', '-'}, {'O', 'O', 'X'}}; 
        Lab3.currentPlayer = 'O';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinCol3By_O_output_true (){
        Lab3.board = new char[][]{{'X', 'O', 'O'}, {'X', 'X', 'O'}, {'O', '-', 'O'}}; 
        Lab3.currentPlayer = 'O';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinDiagonal1By_O_output_true (){
        Lab3.board = new char[][]{{'O', 'O', 'X'}, {'X', 'O', '-'}, {'X', '-', 'O'}}; 
        Lab3.currentPlayer = 'O';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckWinDiagonal2By_X_output_true (){
        Lab3.board = new char[][]{{'O', 'O', 'X'}, {'O', 'X', '-'}, {'X', '-', '-'}}; 
        Lab3.currentPlayer = 'X';
        assertEquals(true, Lab3.hasWon());
    }
    
    @Test
    public void  testCheckDraw_output_false (){
        Lab3.board = new char[][]{{'-', 'O', 'O'}, {'-', '-', '-'}, {'X', 'X', 'X'}}; 
        Lab3.currentPlayer = 'X';
        assertEquals(false, Lab3.isDraw());
    }
    
    @Test
    public void  testCheckDraw_output_true (){
        Lab3.board = new char[][]{{'X', 'O', 'X'},{'X', 'X', 'O'},{'O', 'X', 'O'}}; 
        Lab3.currentPlayer = 'O';
        assertEquals(true, Lab3.isDraw());
    }
    
    
}
